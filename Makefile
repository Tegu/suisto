DEBUG=-DDEBUG -g
CXXFLAGS=-Wall -Wextra -pedantic -std=c++17
EXECUTABLE=suisto

ifndef NOPT
	CXXFLAGS+=-O2
endif

all: $(EXECUTABLE)

debug: CXXFLAGS+=$(DEBUG)
debug: $(EXECUTABLE)

$(EXECUTABLE): $(EXECUTABLE).cpp
	$(CXX) $(CXXFLAGS) $< -o $@

clean:
	$(RM) $(EXECUTABLE)
