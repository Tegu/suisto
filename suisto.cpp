// C++
#include <iostream>
#include <vector>
#include <limits>
#include <algorithm>

struct xy {
	int x, y;

	xy(): x(0), y(0) {}
	xy(int x, int y): x(x), y(y) {}
};

xy operator+ (xy a, xy b) {
	return xy(a.x + b.x, a.y + b.y);
}

struct Siirto {
	int x, y;
	int laatta_id;

	Siirto(): x(0), y(0), laatta_id(0) {}
	Siirto(int x, int y, int laatta_id): x(x), y(y), laatta_id(laatta_id) {}
};

std::vector<xy> operator+ (xy alkupiste, const std::vector<xy> laatta) {
	std::vector<xy> uusi_laatta = laatta;
	for (xy& p : uusi_laatta) {
		p = p + alkupiste;
	}
	return uusi_laatta;
}


class Lauta {
	std::vector<int> lauta;
	int leveys;

	// Funktio pelilaudan ruudun asettamiseen.
	void aseta(int x, int y, int arvo) {
		lauta[(y-1) * leveys + (x-1)] = arvo;
	}

	// Funktio pelilaudan ruudun poistamiseen
	void poista(int x, int y) {
		aseta(x, y, 0);
	}

public:
	Lauta(): lauta(0), leveys(0) {}
	Lauta(int leveys): lauta(leveys*leveys), leveys(leveys) {}

	int ota_leveys() const {
		return leveys;
	}

	// Funktio pelilaudan ruudun käsittelyyn.
	int ruutu(int x, int y) const {
		if (1 <= x && x <= leveys && 1 <= y && y <= leveys) {
			return lauta[(y-1) * leveys + (x-1)];
		}
		return 0;
	}

	// Funktio koko siirron asettamiseen
	// Olettaa, että laatta on kelvollinen
	// sx ja sy liikuttavat kaikkia siirron ruutuja
	void kirjaa(std::vector<xy> siirto, int merkki, int sx=0, int sy=0) {
		for (auto p: siirto) {
			aseta(sx + p.x, sy + p.y, merkki);
		}
	}

	// Funktio ruudun naapureiden tarkastamiseen.
	bool sivut_vapaat(int x, int y) const {
		return ruutu(x+1, y) == 0 && ruutu(x-1, y) == 0 && ruutu(x, y+1) == 0 && ruutu(x, y-1) == 0;
	}

	// Funktio ruudun kulmittaisten naapureiden tarkastamiseen.
	bool kulmittain(int x, int y, int luku) const {
		return ruutu(x+1, y+1) == luku || ruutu(x-1, y+1) == luku || ruutu(x-1, y-1) == luku || ruutu(x+1, y-1) == luku;
	}

	// Funktio pelilaudan kulman tunnistamiseen.
	bool kulmaruutu(int x, int y) const {
		return (x == 1 || x == leveys) && (y == 1 || y == leveys);
	}

	// Funktio tarkistamaan, onko annettu ruutu laudalla
	bool on_laudalla(int x, int y) const {
		return (x >= 1 && x <= leveys) && (y >= 1 && y <= leveys);
	}

	// Funktio laskemaan tietyn pelaajan avoimet kulmittaisruudut lähellä pistettä
	int laske_avoimet_kulmittaiset_lahella(int ox, int oy, int pelinumero) const {
		const int koko = 4;
		int kulmittaisia = 0;
		for (int y = std::max(1, oy - koko); y <= std::min(leveys, oy + koko); ++y) {
			for (int x = std::max(1, ox - koko); x <= std::min(leveys, ox + koko); ++x) {
				if (ruutu(x, y) != 0)
					continue;
				if (!sivut_vapaat(x, y))
					continue;
				if (kulmittain(x, y, pelinumero))
					++kulmittaisia;
			}
		}
		return kulmittaisia;
	}

	// Funktio laskemaan tietyn pelaajan avoimet kulmittaisruudut
	int laske_avoimet_kulmittaiset(int pelinumero) const {
		int kulmittaisia = 0;
		for (int y = 1; y <= leveys; ++y) {
			for (int x = 1; x <= leveys; ++x) {
				if (ruutu(x, y) != 0)
					continue;
				if (!sivut_vapaat(x, y))
					continue;
				if (kulmittain(x, y, pelinumero))
					++kulmittaisia;
			}
		}
		return kulmittaisia;
	}
};

class Tekoaly {
	// Tekoälyn nimi.
	const char* const NIMI = "Suisto";

	// Olennaiset muuttujat.
	Lauta lauta;
	int pelaajia, pelinumero;

	// Kaikki laattavaihdot eri suuntineen ja pyörityksineen
	static const std::vector<std::vector<xy>> LAATAT;

	// Funktio oman siirron valintaan.
	std::vector<xy> valitse(bool aloitus) const {
		std::vector<Siirto> mahdolliset_siirrot = hae_mahdolliset_siirrot(aloitus);
		if (!mahdolliset_siirrot.empty()) {
			Siirto siirto = hae_paras_siirto(mahdolliset_siirrot);
			return xy(siirto.x, siirto.y) + LAATAT[siirto.laatta_id];
		} else {
			return std::vector<xy>();
		}
	}

	// Funktio mahdollisten siirtojen hakemiseen
	std::vector<Siirto> hae_mahdolliset_siirrot(bool aloitus) const {
		std::vector<Siirto> mahdolliset_siirrot;
		const int leveys = lauta.ota_leveys();
		const int laatat_koko = LAATAT.size();
		for (int y = 1; y <= leveys; ++y) {
			for (int x = 1; x <= leveys; ++x) {
				if (lauta.ruutu(x, y) != 0)
					continue;
				if (!lauta.sivut_vapaat(x, y))
					continue;
				if (aloitus && !lauta.kulmaruutu(x, y))
					continue;
				if (aloitus || lauta.kulmittain(x, y, pelinumero)) {
					for (int i = 0; i < laatat_koko; ++i) {
						if (tarkista_laatta(x, y, i)) {
							Siirto siirto(x, y, i);
							mahdolliset_siirrot.push_back(siirto);
						}
					}
				}
			}
		}
		return mahdolliset_siirrot;
	}

	// Funktio laskemaan lautojen kulmaruutujen lukumäärien ero siirron lähellä
	static int avoimien_kulmittaisten_ero(Lauta lauta1, Lauta lauta2, Siirto siirto, int pelaaja) {
		return (lauta2.laske_avoimet_kulmittaiset_lahella(siirto.x, siirto.y, pelaaja)
				- lauta1.laske_avoimet_kulmittaiset_lahella(siirto.x, siirto.y, pelaaja));
	}

	// Funktio laskemaan laudan hyvyys
	int laske_hyvyys(Lauta kopio, Siirto siirto) const {
		int hyvyys = Tekoaly::avoimien_kulmittaisten_ero(lauta, kopio, siirto, pelinumero);
		for (int pelaaja = 1; pelaaja <= pelaajia; ++pelaaja) {
			if (pelaaja != pelinumero) {
				hyvyys -= Tekoaly::avoimien_kulmittaisten_ero(lauta, kopio, siirto, pelaaja);
			}
		}
		return hyvyys;
	}

	// Funktio parhaan laatan hakemiseen
	Siirto hae_paras_siirto(const std::vector<Siirto > siirrot) const {
		Siirto max_siirto;
		int max_hyvyys = std::numeric_limits<int>::lowest();
		Lauta kopio = lauta;
		for (const auto& siirto : siirrot) {
			const auto& laatta = LAATAT[siirto.laatta_id];
			kopio.kirjaa(laatta, pelinumero, siirto.x, siirto.y);
			int hyvyys = laske_hyvyys(kopio, siirto);
			if (hyvyys > max_hyvyys) {
				max_siirto = siirto;
				max_hyvyys = hyvyys;
			}
			kopio.kirjaa(laatta, 0, siirto.x, siirto.y);
		}
		return max_siirto;
	}

	// Funktio tarkistamaan, voiko annettua laattaa asettaa laudalle
	// Olettaa, että x, y on kelvollinen ruutu (muuten pitäisi huomioida aloitus)
	bool tarkista_laatta(int x, int y, int laatta_id) const {
		for (const auto& p : LAATAT[laatta_id]) {
			if (!lauta.on_laudalla(x + p.x, y + p.y))
				return false;
			if (lauta.ruutu(x + p.x, y + p.y) != 0)
				return false;
			if (!lauta.sivut_vapaat(x + p.x, y + p.y))
				return false;
		}
		return true;
	}

public:
	// Tekoälyn pääohjelma.
	void run() {
		int leveys = 0;

		// Tulostetaan oma nimi.
		std::cout << NIMI << std::endl;

		// Luetaan pelilaudan leveys (kilpailussa 16 eli 16x16) ja oma pelinumero (1-3).
		std::cin >> leveys >> pelaajia >> pelinumero;

		// Luodaan pelilauta.
		lauta = Lauta(leveys);

		// Pelataan, kunnes siirtoa ei löydy tai tulee lopetuskäsky (-1).
		for (int laskuri = 0; true; ++laskuri) {
			int vuorossa = 1 + laskuri % pelaajia;
			if (vuorossa == pelinumero) {
				bool aloitus = laskuri < pelaajia;
				std::vector<xy> siirto = valitse(aloitus);
				if (siirto.empty()) {
					break;
				}
				lauta.kirjaa(siirto, pelinumero);
				std::cout << siirto.size();
				for (auto p: siirto) {
					std::cout << ' ' << p.x << ' ' << p.y;
				}
				std::cout << std::endl;
			} else {
				int n;
				std::cin >> n;
				if (n == -1) {
					break;
				}
				std::vector<xy> siirto(n);
				for (auto& p: siirto) {
					std::cin >> p.x >> p.y;
				}
				lauta.kirjaa(siirto, vuorossa);
			}
		}
	}

};

// Laattojen alustus
const std::vector<std::vector<xy>> Tekoaly::LAATAT = {
	{{0,0}},
	{{0,-1},{0,0}},
	{{0,-2},{0,-1},{0,0}},
	{{0,-3},{0,-2},{0,-1},{0,0}},
	{{-1,-2},{0,-2},{0,-1},{0,0}},
	{{0,-2},{1,-2},{0,-1},{0,0}},
	{{-1,-1},{0,-1},{0,0}},
	{{-1,-2},{-1,-1},{0,-1},{0,0}},
	{{0,-2},{-1,-1},{0,-1},{0,0}},
	{{-2,-1},{-1,-1},{0,-1},{0,0}},
	{{0,-1},{1,-1},{0,0}},
	{{0,-2},{0,-1},{1,-1},{0,0}},
	{{1,-2},{0,-1},{1,-1},{0,0}},
	{{-1,-1},{0,-1},{1,-1},{0,0}},
	{{0,-1},{1,-1},{2,-1},{0,0}},
	{{-1,0},{0,0}},
	{{-1,-1},{-1,0},{0,0}},
	{{-1,-2},{-1,-1},{-1,0},{0,0}},
	{{-2,-1},{-1,-1},{-1,0},{0,0}},
	{{0,-1},{-1,0},{0,0}},
	{{0,-2},{0,-1},{-1,0},{0,0}},
	{{-1,-1},{0,-1},{-1,0},{0,0}},
	{{0,-1},{1,-1},{-1,0},{0,0}},
	{{-2,0},{-1,0},{0,0}},
	{{-2,-1},{-2,0},{-1,0},{0,0}},
	{{-1,-1},{-2,0},{-1,0},{0,0}},
	{{0,-1},{-2,0},{-1,0},{0,0}},
	{{-3,0},{-2,0},{-1,0},{0,0}},
	{{0,0},{1,0}},
	{{0,-1},{0,0},{1,0}},
	{{0,-2},{0,-1},{0,0},{1,0}},
	{{-1,-1},{0,-1},{0,0},{1,0}},
	{{1,-1},{0,0},{1,0}},
	{{1,-2},{1,-1},{0,0},{1,0}},
	{{0,-1},{1,-1},{0,0},{1,0}},
	{{1,-1},{2,-1},{0,0},{1,0}},
	{{0,0},{1,0},{2,0}},
	{{0,-1},{0,0},{1,0},{2,0}},
	{{1,-1},{0,0},{1,0},{2,0}},
	{{2,-1},{0,0},{1,0},{2,0}},
	{{0,0},{1,0},{2,0},{3,0}},
	{{-2,0},{-1,0},{0,0},{-2,1}},
	{{-1,0},{0,0},{-1,1}},
	{{-1,-1},{-1,0},{0,0},{-1,1}},
	{{0,-1},{-1,0},{0,0},{-1,1}},
	{{-2,0},{-1,0},{0,0},{-1,1}},
	{{-1,0},{0,0},{-2,1},{-1,1}},
	{{0,0},{0,1}},
	{{-1,0},{0,0},{0,1}},
	{{-1,-1},{-1,0},{0,0},{0,1}},
	{{-2,0},{-1,0},{0,0},{0,1}},
	{{0,0},{1,0},{0,1}},
	{{1,-1},{0,0},{1,0},{0,1}},
	{{0,0},{1,0},{2,0},{0,1}},
	{{0,0},{-1,1},{0,1}},
	{{-1,0},{0,0},{-1,1},{0,1}},
	{{0,0},{1,0},{-1,1},{0,1}},
	{{0,0},{-2,1},{-1,1},{0,1}},
	{{0,0},{1,0},{1,1}},
	{{0,-1},{0,0},{1,0},{1,1}},
	{{1,-1},{0,0},{1,0},{1,1}},
	{{0,0},{1,0},{2,0},{1,1}},
	{{0,0},{0,1},{1,1}},
	{{-1,0},{0,0},{0,1},{1,1}},
	{{0,0},{1,0},{0,1},{1,1}},
	{{0,0},{-1,1},{0,1},{1,1}},
	{{0,0},{1,0},{2,0},{2,1}},
	{{0,0},{1,0},{1,1},{2,1}},
	{{0,0},{0,1},{1,1},{2,1}},
	{{-1,0},{0,0},{-1,1},{-1,2}},
	{{0,0},{-1,1},{0,1},{-1,2}},
	{{0,0},{0,1},{0,2}},
	{{-1,0},{0,0},{0,1},{0,2}},
	{{0,0},{1,0},{0,1},{0,2}},
	{{0,0},{-1,1},{0,1},{0,2}},
	{{0,0},{0,1},{1,1},{0,2}},
	{{0,0},{0,1},{-1,2},{0,2}},
	{{0,0},{1,0},{1,1},{1,2}},
	{{0,0},{0,1},{1,1},{1,2}},
	{{0,0},{0,1},{0,2},{1,2}},
	{{0,0},{0,1},{0,2},{0,3}},
};

int main() {
	Tekoaly t;
	t.run();
}
