Suisto

Hölmö pikkuäly, joka haarautuu kuin jokisuisto ikään. Yrittää luoda joka vuorolla mahdollisimman paljon uusia avoimia kulmittaisruutuja. Siten se tuhlaa välillä omia paikkojaan turhan suurilla laatoilla. Antaa toisaalta pisteitä myös sille, että laatta tukkii kilpailijan avoimen ruudun. Vuoroja ei lasketa eteenpäin. Tulos tuntuu riippuvan turhan paljon laattojen tutkimisjärjestyksestä ja aloitusnurkasta. Pohjana on käytetty esimerkkiohjelmaa.

Testattu GCC:n versioilla 6.3.0 (Debian 9) ja 7.1.0 (Windows 10). Mukana on myös jonkinlainen Makefile, vaikka toisaalta kaikki koodi onkin ängetty yhteen tiedostoon.

Tehty Ohjelmointiputkan Plokkis-kilpailua varten: https://www.ohjelmointiputka.net/kilpa.php?tunnus=plokkis
